package com.home.service;

import com.home.domain.Note;
import com.home.domain.Tag;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;


@Stateless
public class NotesService {

  @PersistenceContext(unitName = "defaultPU")
  EntityManager entityManager;

  public Note createNote(String title, String body, String [] tags){
    Note note = new Note(title, body);
    for(Tag tag : parseTags(tags)){
      if (getTagByDescription(tag.getDescription()) == null){
        entityManager.persist(tag);
        note.getTags().add(tag);
        tag.getNotes().add(note);
      } else {
        note.getTags().add(getTagByDescription(tag.getDescription()));
        getTagByDescription(tag.getDescription()).getNotes().add(note);
      }
    }
    entityManager.persist(note);
    entityManager.flush();
    return note;
  }

  public ArrayList<Tag> parseTags(String [] tags){
    ArrayList<Tag> noteTags = new ArrayList<>();
    int i;
    for (i = 0; i < tags.length; i++){
      Tag addedTag = new Tag(tags[i]);
      noteTags.add(addedTag);
    }
    return noteTags;
  }

  public Note readNote(Long id){
    return entityManager.find(Note.class, id);
  }

  public Note updateNote(Long id, String title, String body) {
    Note findedNote = entityManager.find(Note.class, id);
    if (findedNote != null) {
      findedNote.setTitle(title);
      findedNote.setBody(body);
    }
    entityManager.persist(findedNote);
    return findedNote;
  }

  public Note deleteNote(Long id){
    Note findedNote = entityManager.find(Note.class, id);
    if (findedNote != null)
      entityManager.remove(findedNote);
    return findedNote;
  }

  public Note getById(Long id) {
    return entityManager.find(Note.class, id);
  }

  public List<Note> getAllNotes(){
    List<Note> allNotes = (List) entityManager.createQuery("select e from Note e").getResultList();
    return allNotes;
  }

  public Tag getTagByDescription(String description){
    List<Tag> tags = entityManager.
            createQuery("FROM Tag as t WHERE t.description = '" + description + "'").getResultList();
    if (tags.size() == 0)
      return null;
    if (tags.size() != 0)
      return tags.get(0);
    return null;
  }
}
