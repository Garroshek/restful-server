package com.home.rest;

public class NotesRequest {
    private String title;
    private String body;
    private String [] tags;

    public NotesRequest() {
    }

    public NotesRequest(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
