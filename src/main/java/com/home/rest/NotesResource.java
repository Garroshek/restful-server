package com.home.rest;

import com.home.domain.Note;
import com.home.file.UploadFiles;
import com.home.service.NotesService;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Path("notes")
public class NotesResource {

  @Inject
  NotesService service;

  @POST
  @Produces("application/json")
  @Consumes("application/json")
  public Note createNote(NotesRequest notesRequest){
    return service.createNote(notesRequest.getTitle(), notesRequest.getBody(), notesRequest.getTags());
  }

  @GET
  @Path("/{id}")
  @Produces("application/json")
  public Note readNote(@PathParam("id") Long id){
    return service.readNote(id);
  }

  @PUT
  @Path("/{id}")
  @Consumes("application/json")
  @Produces("application/json")
  public Note updateNote(@PathParam("id") Long id, NotesRequest notesRequest){
    return service.updateNote(id, notesRequest.getTitle(), notesRequest.getBody());
  }

  @DELETE
  @Path("/{id}")
  @Produces("application/json")
  public Note deleteNote(@PathParam("id") Long id){
    return service.deleteNote(id);
  }

  @GET
  @Produces("application/json")
  public List<Note> getAllNotes() {
   List<Note> allNotes = service.getAllNotes();
    return allNotes;
  }
}
